/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeszk_grapheditor.algorithm;

import java.util.ArrayList;
import java.util.List;
import javafx.scene.paint.Color;
import projeszk_grapheditor.data.Graph;
import projeszk_grapheditor.data.GraphEdge;
import projeszk_grapheditor.data.GraphNode;

/**
 * A Dijkstra algoritmus osztálya 
 * <p>
 * Az algoritmus megkeresi és bejelöli a gráfon a legrövidebb utat a kezdő csúcstól az összes többi csúcsig
 * 
 * @author Komáromi Zsófia
 * @author Pollák József
 */
public class Dijkstra {

    public static void DijkstraAlgorithm(Graph graph) {
        double inf = Double.POSITIVE_INFINITY;
        graph.setAllEdgeColor(Color.BLACK);
        List<GraphNode> nodes = graph.getNodes();
        //ha 1-nél több noda van, és ha a start node-ból van kiinduló edge
        if (graph.getNodes().size() > 1 && graph.getNodes().get(0).getOutSize() > 0) {
            //csucsok tömbje
            List<Integer> mainNodes = new ArrayList<>(graph.getNodes().size());
            //távolságok (starttól) tömbje
            List<Double> weights = new ArrayList<>(graph.getNodes().size());
            //megelőző csúcsok tömbje
            List<Integer> predNodes = new ArrayList<>(graph.getNodes().size());
            //kész csúcsok
            List<Integer> doneNodes = new ArrayList<>(graph.getNodes().size());

            System.out.println("Dijkstra");

            /* Inicializálás:
             * StartNode = mainNode.get(0);
             * mainNodes: [id0 id1 ... idn]
             * weights:   [0   inf ... inf]
             * predNodes: [-1    0 ...   0]
             * doneNodes: [0     0 ...   0]
             */
            for (int i = 0; i < nodes.size(); i++) {
                mainNodes.add(nodes.get(i).getID());
            }

            weights.add(0, 0.0);
            for (int i = 1; i < nodes.size(); i++) {
                weights.add(i, inf);
            }

            predNodes.add(0, -1);
            for (int i = 1; i < nodes.size(); i++) {
                predNodes.add(i, 0);
            }

            for (int i = 0; i < nodes.size(); i++) {
                doneNodes.add(i, 0);
            }

            System.out.println("Inicializálás kész!");
            System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~");

            System.out.println("mainNodes:");
            for (int i = 0; i < mainNodes.size(); i++) {
                System.out.println(mainNodes.get(i));
            }
            System.out.println("weights:");
            for (int i = 0; i < weights.size(); i++) {
                System.out.println(weights.get(i));
            }
            System.out.println("predNodes:");
            for (int i = 0; i < predNodes.size(); i++) {
                System.out.println(predNodes.get(i));
            }
            System.out.println("doneNodes:");
            for (int i = 0; i < doneNodes.size(); i++) {
                System.out.println(doneNodes.get(i));
            }

            System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            System.out.println("Algoritmus kezdete");
            //megnézem a start csúcsot, hogy hány él fut ki belőle
            //lekérem a kifutó éleket a startból, és veszem az elsőt
            //és a távolságot beírom a helyére, ha kisebb, mint ami már ott van
            int index = 0;
            for (int i = 0; i < graph.getNodes().get(0).getOutSize(); i++) {
                GraphEdge edge = graph.getNodes().get(0).getOut().get(i);
                for (int j = 0; j < mainNodes.size(); j++) {
                    if (mainNodes.get(j) == edge.getTo().getID()) {
                        index = j;
                    }
                }

                if (edge.getWeight() < weights.get(index)) {
                    weights.set(index, edge.getWeight());
                    predNodes.set(index, edge.getFrom().getID());
                }

            }
            doneNodes.set(0, graph.getNodes().get(0).getID());

            System.out.println("Vizsgálat Starttól:");
            System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~");

            System.out.println("mainNodes:");
            for (int i = 0; i < mainNodes.size(); i++) {
                System.out.println(mainNodes.get(i));
            }
            System.out.println("Élek: ");
            for (int i = 0; i < graph.getEdges().size(); i++) {
                System.out.println(i + ".: " + graph.getEdges().get(i).getWeight());
            }
            System.out.println("weights:");
            for (int i = 0; i < weights.size(); i++) {
                System.out.println(weights.get(i));
            }
            System.out.println("predNodes:");
            for (int i = 0; i < predNodes.size(); i++) {
                System.out.println(predNodes.get(i));
            }
            System.out.println("doneNodes:");
            for (int i = 0; i < doneNodes.size(); i++) {
                System.out.println(doneNodes.get(i));
            }

            System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~");

            for (int cs = 0; cs < (mainNodes.size() - 1); cs++) {

                //megkeresem a legkisebb súlyt, a nem kész csúcsok közül
                int minInd = 0;
                double min = inf;
                for (int i = 0; i < doneNodes.size(); i++) {
                    if (doneNodes.get(i) == 0) {
                        if (weights.get(i) < min) {
                            min = weights.get(i);
                            minInd = i;
                        }
                    }
                }

                System.out.println("MinInd: " + minInd + " Weight: " + min);
                if (min == inf) {
                    System.out.println("A maradék csúcs(ok)ba nem lehet eljutni a kezdőponttól");
                } else {
                    index = 0;
                    for (int i = 0; i < graph.getNodes().get(minInd).getOutSize(); i++) {
                        GraphEdge edge = graph.getNodes().get(minInd).getOut().get(i);
                        for (int j = 0; j < mainNodes.size(); j++) {
                            if (mainNodes.get(j) == edge.getTo().getID()) {
                                //a To csúcs hely a csúcsok listában
                                index = j;
                            }
                        }

                        if ((weights.get(minInd) + edge.getWeight()) < weights.get(index)) {
                            weights.set(index, (weights.get(minInd) + edge.getWeight()));
                            predNodes.set(index, edge.getFrom().getID());
                        }

                    }
                    doneNodes.set(minInd, graph.getNodes().get(minInd).getID());

                    System.out.println("Vizsgálat Minimum:");
                    System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~");

                    System.out.println("mainNodes:");
                    for (int i = 0; i < mainNodes.size(); i++) {
                        System.out.println(mainNodes.get(i) + ", " + graph.getNodes().get(i).getLabel());
                    }
                    System.out.println("weights:");
                    for (int i = 0; i < weights.size(); i++) {
                        System.out.println(weights.get(i));
                    }
                    System.out.println("predNodes:");
                    for (int i = 0; i < predNodes.size(); i++) {
                        System.out.println(predNodes.get(i));
                    }
                    System.out.println("doneNodes:");
                    for (int i = 0; i < doneNodes.size(); i++) {
                        System.out.println(doneNodes.get(i));
                    }

                    System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                }
                
            }
            for (int i = 1; i < doneNodes.size(); i++) {
                if(doneNodes.get(i) != 0) {
                    for (int j = 0; j < graph.getEdges().size(); j++) {
                        if(graph.getEdges().get(j).getFrom().getID() == predNodes.get(i)  && graph.getEdges().get(j).getTo().getID() == doneNodes.get(i)) {

                            graph.getEdges().get(j).setColor(Color.RED);

                           // graph.getEdges().get(j).setColor(2);

                        }
                    }
                }
            }
        } else {
            System.out.println("Nincs kimenő él a startból!");
        }

    }
}
