/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package projeszk_grapheditor;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * <b>Gráfszerkesztő</b>
 * 
 * Ezt a programot gráfok rajzolására és gráfalgoritmusok szemléltetésére lehet használni. Irányítatlan, súlyozatlan gráfokat tud rajzolni, a csúcsokat sorrendben számozza meg. 
 * Lehet éleket és/vagy csúcsokat törölni illetve áthelyezni. A készített gráfot mindig eltárolja (ez akkor működik, ha az swf fájlt a merevlemezre mentjük). Ha új gráfot szeretnénk, az Új gráf gombbal törölhetünk.
 * Ezen kívül a szélességi és a mélységi gráfbejárást lehet szemléltetni. Ilyenkor első lépésben ki kell jelölni egy kezdőcsúcsot, majd az algoritmus léptetéséhez kattintani a színtéren. A program mutatja az érintett, feldolgozott csúcsokat és éleket. 
 * A bal alsó sarokban egy szövegbuborékban segítséget kaphatunk az éppen használt parancshoz. Kattintással nagyíthatjuk ki illetve kicsinyíthetjük le.
 * Használat
 * Gyorsbillentyűk nincsenek. Az aktív gomb zöld színű. A csúcsokat csak akkor lehet mozgatni, ha minden gomb inaktív.
 * 
 * <p>A programot gráfok rajzolására, szerkesztésére és a Dijkstra algoritmus <i>(A legrövidebb utak kiszámítása)</i> szemléltetésére lehet használni. </p>
 * 
 * <p> Irányított, súlyozott gráfokat tud rajzolni. A <b>csúcsokat</b> az angol abc betűinek sorrendjében cimkézi meg. A csúcsok törölhetők, illetve átmozgathatók.
 * A már éllel rendelkező csúcsok törlésekor törlődnek a hozzátartozó élek is. A <b>súlyokat</b> az élekhez a program véletlenszerűen generálja.
 * A rajzolási módokat (csúcs vagy él rajzolása) az ablak fenti részén lévő gombok segítségével változtathatjuk meg.
 * Az ablak jobb oldalán található egy információs panel, ahol a felhasználó segítséget kap a billentyűkombinációkhoz, illetve információkat a rajzolt gráfról. </p>
 * 
 * <p>Az algoritmus a jobb oldali panelen lévő kapcsolóra kattintva fut le és akkor rajzolódik ki az átalakított gráf.</p>
 * 
 * @author Pollák József
 * @author Komáromi Zsófia
 */
public class ProjEszk_GraphEditor extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/projeszk_grapheditor/GUI/FXMLMain.fxml"));
        
        Scene scene = new Scene(root);
        
        scene.getStylesheets().add("/projeszk_grapheditor/GUI/myCSS.css");
        
        stage.setScene(scene);
        stage.setResizable(false);
        stage.setWidth(800);
        stage.setHeight(600);
        stage.show();
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
