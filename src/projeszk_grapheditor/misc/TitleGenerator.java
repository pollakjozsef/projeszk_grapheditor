/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package projeszk_grapheditor.misc;

/**
 *
 * @author Pollák József
 * @author Komáromi Zsófia
 */
public class TitleGenerator{
    private static int character=65;
    
    public static char getNext(){
        char ch = (char)character;
        character=(((character-65)+1)%26)+65;
        return ch;
    }
    
    private TitleGenerator(){
        throw new UnsupportedOperationException();
    }
    
}
