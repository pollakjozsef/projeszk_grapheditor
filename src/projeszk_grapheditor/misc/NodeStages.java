/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package projeszk_grapheditor.misc;

/**
 * Csúcs állapotok
 *
 * @author Pollák József
 * @author Komáromi Zsófia
 */
public enum NodeStages {EMPTY,MOVE_VIEW, MOVE_NODE, ADD_NODE, ADD_EDGE}
