/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package projeszk_grapheditor.misc;

/**
 * Az ablak felső részén lévő gombok 
 *
 * @author Pollák József
 * @author Komáromi Zsófia
 */
public enum EditModes{NODE, EDGE, DUMMY}
