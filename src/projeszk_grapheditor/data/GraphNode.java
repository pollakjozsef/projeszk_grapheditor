/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeszk_grapheditor.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.util.Duration;

/**
 * A csúcs osztály 
 * 
 * @author Pollák József
 * @author Komáromi Zsófia
 */
public class GraphNode{

    private final Circle circle;
    private final Circle selectionCircle;
    private final Label label;
    private boolean selected;
    private final int ID;
    private final List<GraphEdge> in;
    private final List<GraphEdge> out;

    /**
     * A csúcs osztály konstruktora
     * 
     * @param x helye az ablakban x mentén
     * @param y helye az ablakban y mentén
     * @param diameter a kör/csúcs átmérője
     * @param labelText a csúcs felirata
     * @param ID a csúcs azonosítója
     */
    public GraphNode(double x, double y, double diameter, String labelText, int ID){
        in=new ArrayList<>(10);
        out=new ArrayList<>(10);
        this.ID=ID;
        selected=false;
        this.circle=new Circle(x, y, diameter);
        this.selectionCircle=new Circle(x, y, diameter);
        circle.getStyleClass().add("inner-circle");
        selectionCircle.getStyleClass().add("outer-circle");
        selectionCircle.centerXProperty().bind(circle.centerXProperty());
        selectionCircle.centerYProperty().bind(circle.centerYProperty());

        circle.setOnMouseEntered(new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent e){
                if(!selected){
                    final Timeline timeline=new Timeline();
                    final KeyValue kv=new KeyValue(selectionCircle.radiusProperty(), circle.getRadius()*1.2);
                    final KeyFrame kf=new KeyFrame(Duration.millis(100), kv);
                    timeline.getKeyFrames().add(kf);
                    timeline.play();
                }
            }
        });
        circle.setOnMouseExited(new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent e){

                if(!selected){
                    final Timeline timeline=new Timeline();
                    final KeyValue kv=new KeyValue(selectionCircle.radiusProperty(), circle.getRadius());
                    final KeyFrame kf=new KeyFrame(Duration.millis(100), kv);
                    timeline.getKeyFrames().add(kf);
                    timeline.play();
                }
            }
        });

        this.label=new Label(labelText);
        label.setLayoutX(x+label.getWidth()/2);
        label.setLayoutY(y+label.getHeight()/2);
        label.getStyleClass().add("label-circle");
        label.setMouseTransparent(true);
//            label.layoutXProperty().bind(circle.layoutXProperty());
//            label.layoutYProperty().bind(circle.layoutYProperty());

        circle.setScaleX(0);
        circle.setScaleY(0);
        selectionCircle.setScaleX(0);
        selectionCircle.setScaleY(0);
        final Timeline timeline1=new Timeline();
        KeyValue kv1=new KeyValue(circle.scaleXProperty(), 1.2);
        KeyValue kv2=new KeyValue(circle.scaleYProperty(), 1.2);
        KeyValue kv3=new KeyValue(selectionCircle.scaleXProperty(), 1.2);
        KeyValue kv4=new KeyValue(selectionCircle.scaleYProperty(), 1.2);
        KeyFrame kf1=new KeyFrame(Duration.millis(80), kv1, kv2, kv3, kv4);
        timeline1.getKeyFrames().add(kf1);
        timeline1.play();

        timeline1.setOnFinished(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent event){
                final Timeline timeline2=new Timeline();
                KeyValue kv5=new KeyValue(circle.scaleXProperty(), 1);
                KeyValue kv6=new KeyValue(circle.scaleYProperty(), 1);
                KeyValue kv7=new KeyValue(selectionCircle.scaleXProperty(), 1);
                KeyValue kv8=new KeyValue(selectionCircle.scaleYProperty(), 1);
                KeyFrame kf2=new KeyFrame(Duration.millis(50), kv5, kv6, kv7, kv8);
                timeline2.getKeyFrames().add(kf2);
                timeline2.play();
                timeline2.setOnFinished(new EventHandler<ActionEvent>(){

                    @Override
                    public void handle(ActionEvent event){
                        repositionLabel();
                    }
                });
            }
        });

    }

    /**
     * Az él törlése 
     * 
     * @param e
     */
    protected void removeEdge(GraphEdge e){
        boolean a = in.remove(e);
        boolean b = out.remove(e);
    }

    /**
     * A csúcshoz tartozó (kiinduló) él
     * 
     * @param e
     */
    protected void addEdgeIn(GraphEdge e){
        in.add(e);
             
    }

    /**
     * A csúcshoz tartozó (beérkező) él
     * 
     * @param e
     */
    protected void addEdgeOut(GraphEdge e){
        out.add(e);
       
    }

    /**
     * Visszaadja a csúcshoz tartozó kiindulási élt
     *
     * @return
     */
    public List<GraphEdge> getIn(){
        return in;
    }

    /**
     *Visszaadja a csúcshoz tartozó beérkező élt
     * 
     * @return
     */
    public List<GraphEdge> getOut(){
        return out;
    }
    
    /**
     * Egy csúcsból kimenő élek száma
     * 
     * @return élek száma
     */
    public int getOutSize() {
        return out.size();
    }

    /**
     * A cimke pozícionálása
     */
    public void repositionLabel(){
        label.setLayoutX(circle.getCenterX()-label.getWidth()/2);
        label.setLayoutY(circle.getCenterY()-label.getHeight()/2);
    }

    /**
     * Visszaadja a csúcsot reprezentáló kört
     * 
     * @return
     */
    public Circle getCircle(){
        return circle;
    }

    /**
     * Visszaadja a csúcshoz tartozó cimkét
     * 
     * @return
     */
    public Label getLabel(){
        return label;
    }
    
    /**
     *Visszaadja a csúcshoz tartozó azonosítót
     * 
     * @return
     */
    public int getID(){
        return ID;
    }

     /**
     * Visszaadja a csúcshoz tartozó cimke szövegét
     * 
     * @return
     */
    public String getLabelText(){
        return label.getText();
    }

     /**
     * Beállítja a csúcshoz tartozó cimke szövegét
     * 
     * @param text szöveg
     */
    public void setLabelText(String text){
        this.label.setText(text);
    }

    /**
     * Visszaadja az összes csúcsot
     * 
     * @return
     */
    public List<? extends Node> allNodes(){
        return Arrays.asList(selectionCircle, circle, label);
    }

    @Override
    public int hashCode(){
        int hash=5;
        hash=71*hash+Objects.hashCode(this.circle);
        return hash;
    }

    @Override
    public boolean equals(Object obj){
        if(obj==null){
            return false;
        }
        if(getClass()!=obj.getClass()){
            return false;
        }
        final GraphNode other=(GraphNode)obj;
        if(!Objects.equals(this.circle, other.circle)){
            return false;
        }
        return Objects.equals(this.label, other.label);
    }

    public void setSelected(boolean selected){
        this.selected=selected;
        selectionCircle.setRadius(circle.getRadius()*1.2);
        selectionCircle.setRadius(circle.getRadius()*1.2);
    }

    @Override
    public String toString(){
        return "graphicNode{ Text ="+label.getText()+'}';
    }

    public void unselect(){
        selected=false;
        final Timeline timeline=new Timeline();
        final KeyValue kv=new KeyValue(selectionCircle.radiusProperty(), circle.getRadius());
        final KeyFrame kf=new KeyFrame(Duration.millis(100), kv);
        timeline.getKeyFrames().add(kf);
        timeline.play();
    }

    public boolean isSelected(){
        return selected;
    }

    public double getCenterX(){
        return circle.getCenterX();
    }

    public double getCenterY(){
        return circle.getCenterY();
    }

//    public void setCenterX(double d){
//        circle.setCenterX(d);//selectionCircle is binded!
//        repositionLabel();
//    }
//
//    public void setCenterY(double d){
//        circle.setCenterY(d);
//        repositionLabel();
//    }
    boolean containsCircle(Circle c){
        return circle==c||selectionCircle==c;
    }

    /**
     * Csúcs törlésének szimulálása animációval
     */
    public void removeAnimation(){
        for(GraphEdge ge : in){
            final Timeline timeline1=new Timeline();
            Line l=ge.getLine();
            l.endXProperty().unbind();
            l.endYProperty().unbind();
            KeyValue kv1=new KeyValue(l.endXProperty(), l.getStartX());
            KeyValue kv2=new KeyValue(l.endYProperty(), l.getStartY());
            KeyFrame kf1=new KeyFrame(Duration.millis(200), kv1, kv2);
            timeline1.getKeyFrames().add(kf1);
            timeline1.play();
        }

        for(GraphEdge ge : out){
            final Timeline timeline1=new Timeline();
            Line l=ge.getLine();
            l.startXProperty().unbind();
            l.startYProperty().unbind();
            KeyValue kv1=new KeyValue(l.startXProperty(), l.getEndX());
            KeyValue kv2=new KeyValue(l.startYProperty(), l.getEndY());
            KeyFrame kf1=new KeyFrame(Duration.millis(200), kv1, kv2);
            timeline1.getKeyFrames().add(kf1);
            timeline1.play();
        }

        final Timeline timeline1=new Timeline();
        KeyValue kv1=new KeyValue(circle.scaleXProperty(), 0);
        KeyValue kv2=new KeyValue(circle.scaleYProperty(), 0);
        KeyValue kv3=new KeyValue(selectionCircle.scaleXProperty(), 0);
        KeyValue kv4=new KeyValue(selectionCircle.scaleYProperty(), 0);
        KeyValue kv5=new KeyValue(label.scaleXProperty(), 0);
        KeyValue kv6=new KeyValue(label.scaleYProperty(), 0);
        KeyFrame kf1=new KeyFrame(Duration.millis(100), kv1, kv2, kv3, kv4, kv5, kv6);
        timeline1.getKeyFrames().add(kf1);
        timeline1.play();
    }

    public void relocate(double x, double y){
        circle.setCenterX(x);
        circle.setCenterY(y);
        repositionLabel();
    }

    List<GraphEdge> getEdges(){
        List<GraphEdge> newList=new ArrayList<>();
        newList.addAll(in);
        newList.addAll(out);
        return newList;
    }

    public void relocateAllEdges(){
        in.stream().forEach(e -> e.relocate());
        out.stream().forEach(e -> e.relocate());
    }

}
