
package projeszk_grapheditor.data;

/**
 * Az él típusának meghatározása
 * 
 * @author Pollák József
 * @author Komáromi Zsófia
 */
public enum Type {UNDIRECTED_UNWEIGHTED, DIRECTED_UNWEIGHTED, UNDIRECTED_WEIGHTED, DIRECTED_WEIGHTED}