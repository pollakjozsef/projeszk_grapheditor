package projeszk_grapheditor.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

/**
 * 
 * Ez az osztály magába foglalja mindazt, ami a gráf megvalósításához szükséges:
 * <p>
 * <ul>
 * <li> Csúcsok: {@link Graph#nodes}
 * <li> Élek: {@link Graph#edges}
 * </ul>
 *
 * @author Pollák József
 * @author Komáromi Zsófia
 */
public class Graph {

    //2 konstans, n és e
    public static final List<Integer> ids = new ArrayList<>(50);
    private final List<GraphNode> nodes;
    private final List<GraphEdge> edges;

    /**
     * Graph osztály konstruktora
     */
    public Graph() {
        nodes = new ArrayList<>(50);//n
        edges = new ArrayList<>(50);//e
    }

    /**
     * A Graph(gráf) osztály copy konstruktora
     * 
     * @param graph egy Graph típusú objektumot vár
     */
    public Graph(Graph graph) {
        nodes = new ArrayList<>(graph.getNodes());
        edges = new ArrayList<>(graph.getEdges());
    }

    
    /**
     * Élek hozzáadása a Graph osztályhoz
     * 
     * @param from      a from paraméter az él kiinduló csúcsa
     * @param to        a to paraméter az él beérkező csúcsa
     * @param weight    a weight paraméter az él súlya
     * @param type      a type paraméter az él típusa
     * @param color
     */
    public void addEdge(GraphNode from, GraphNode to, double weight, Type type){
        GraphEdge ge=new GraphEdge(from, to, weight, type);
        addEdge(ge);
    }

    /**
     * Élek hozzáadása
     * 
     * @param ge már egy létező ér kap paraméterként
     */
    public void addEdge(GraphEdge ge) {
        edges.add(ge);
        ge.getFrom().addEdgeOut(ge);
        ge.getTo().addEdgeIn(ge);
    }

    /**
     * A csúcs hozzáadása a nodes {@link Graph#nodes} listához
     * 
     * @param gn egy csúcs
     */
    public void addNode(GraphNode gn) {
        nodes.add(gn);
    }

    /**
     * A gráfhoz tartozó összes csúcs lekérése
     * 
     * @return csúcsok listája {@link Graph#nodes}
     */
    public List<GraphNode> getNodes() {
        return nodes;
    }

    /**
     * A gráfhoz tartozó összes él lekérése
     * 
     * @return csúcsok listája {@link Graph#edges}
     */
    public List<GraphEdge> getEdges() {
        return edges;
    }

    /**
     * Ez a függvény egyedi, nem ismétlődő azonosítót generál a csúcsokhoz
     * 
     * @return genID azonosító
     */
    public static int generateID() {
        Random r = new Random(System.currentTimeMillis());
        int genID = (10000 + r.nextInt(30000));
        while (ids.contains(genID)) {
            genID = (10000 + r.nextInt(30000));
        }
        ids.add(genID);
        return genID;
    }

    /**
     * Visszaadja a kijelölt csúcsot
     * 
     * @param c a kijelölt kör
     * @return a kijelölt csúcs vagy null, ha nincs találat
     */
    public GraphNode getSelected(Circle c) {
        for (GraphNode gn : nodes) {
            if (gn.containsCircle(c)) {
                return gn;
            }
        }
        return null;
    }

    /**
     * Lekéri, hogy a gráfban az élek listájában megtalálható-e a keresett él
     * 
     * @param ge a keresett él
     * @return igaz vagy hamis érték
     */
    public boolean containsEdge(GraphEdge ge) {
        return edges.contains(ge);
    }


    /**
     * Kitörli azokat a Node-okat (mind a grafikai megjelenítésüket az AnchorPane-ből, mind az adatszerkezetből),
     * melynek grafikai elemeinek a skálája egyenlő nullával. (Törlés animáció végén lesz nulla a skála)
     * 
     * @param graphPane 
     */
    public void removeZeroScale(AnchorPane graphPane) {
        for (int i = 0; i < nodes.size(); ++i) {
            GraphNode gn = nodes.get(i);
            if (gn.getCircle().scaleXProperty().get() == 0) {

                nodes.remove(gn);
                graphPane.getChildren().removeAll(gn.allNodes());
                gn.getIn().stream().forEach((e) -> graphPane.getChildren().removeAll(e.allNodes()));
                gn.getOut().stream().forEach((e) -> graphPane.getChildren().removeAll(e.allNodes()));
            }
        }
    }

    /**
     * Ha törlünk egy csúcsot, akkor törlődik a hozzá tartozó összes él is.
     * 
     * @param ge a törölt csúcs
     * @param parent a panel, amiből kitöröljük
     */
    public void removeEdges(GraphNode ge, Pane parent) {
        for (GraphEdge e : ge.getIn()) {
            parent.getChildren().removeAll(e.allNodes());
        }
        for (GraphEdge e : ge.getOut()) {
            parent.getChildren().removeAll(e.allNodes());
        }
    }

    /**
     * Csúcs törlése
     * 
     * @param gn törölni kívánt csúcs
     */
    public void removeNode(GraphNode gn) {
        nodes.remove(gn);
        for (GraphEdge ge : gn.getEdges()) {
            ge.getFrom().removeEdge(ge);
            ge.getTo().removeEdge(ge);
            edges.remove(ge);
        }
    }

    /**
     * Ha a kurzor egy kör (csúcs) fölé kerül, akkor visszakapjuk, hogy melyik csúcs fölé
     * 
     * @param clickX
     * @param clickY 
     * @param circleDiameter a kör átmérője
     * @return a csúcs
     */
    public GraphNode overNode(double clickX, double clickY, double circleDiameter) {
        for (GraphNode gn : nodes) {
            //if(Math.sqrt(Math.pow(gn.getCenterX()-clickX, 2)+Math.pow(gn.getCenterY()-clickY, 2))<=circleDiameter){
            if (gn.getCircle().contains(clickX, clickY)) {
                return gn;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (GraphNode gn : nodes) {
            sb.append(gn.getLabelText()).append("\n");
            sb.append("\nIn:\n");
            for (GraphEdge ge : gn.getIn()) {
                sb.append(ge.getFrom().getLabelText());
            }
            sb.append("\nOut:\n");
            for (GraphEdge ge : gn.getOut()) {
                sb.append(ge.getTo().getLabelText());
            }
            sb.append("\n~~~~~\n");

        }
        return sb.toString();
    }
    
    public void setAllEdgeColor(Color c){
        edges.stream().forEach(e -> e.setColor(c));
    }

}
