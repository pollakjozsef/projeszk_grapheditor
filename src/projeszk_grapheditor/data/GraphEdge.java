package projeszk_grapheditor.data;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;

public final class GraphEdge{

/**
 * Az él osztály
 * 
 * @author Pollák József
 * @author Komáromi Zsófia
 */
    private final Type type;
    private final GraphNode from, to;
    private final double weight;
    private final Line line;
    private final Circle weightCircle;
    private final Label weightLabel;
    private final BorderPane bp;
    private final Polygon p;
    //ha 1, akkor fekete, ha 2 akkor piros a dijkstrahoz



    
    /**
     * Konstruktor
     * 
     * @param from kiinduló csúcs
     * @param to beérkező csúcs
     * @param weight súly
     * @param type él típusa
     */
    public GraphEdge(GraphNode from, GraphNode to, double weight, Type type){
        this.from=from;
        this.to=to;
        this.weight=weight;
        this.type=type;
        line=new Line(from.getCenterX(), from.getCenterY(), to.getCenterX(), to.getCenterY());
        line.startXProperty().bind(from.getCircle().centerXProperty());
        line.startYProperty().bind(from.getCircle().centerYProperty());
        line.endXProperty().bind(to.getCircle().centerXProperty());
        line.endYProperty().bind(to.getCircle().centerYProperty());
        line.setFill(Color.BLACK);

        weightCircle=new Circle(15);

        weightCircle.getStyleClass().add("inner-circle");
        weightLabel=new Label(weight+"");
        p=new Polygon();
        
        //weightLabel.impl_processCSS(true);
 //System.err.println(weightLabel.prefWidth(-1)+"/"+weightLabel.prefHeight(-1));
        
        bp = new BorderPane(weightLabel);
        relocate();

    }


    public void repositionLabel(){
        weightLabel.setLayoutX(weightCircle.getCenterX()-weightLabel.getWidth()/2);
        weightLabel.setLayoutY(weightCircle.getCenterY()-weightLabel.getHeight()/2);
    }


    
    /**
     * Visszaadja az összes csúcsot
     * 
     * @return
     */
    public List<? extends Node> allNodes(){
        return Arrays.asList(line, p, weightCircle, weightLabel);
    }


    /**
     * Visszaadja azt a csúcsot, amiből kiindul az él
     * 
     * @return
     */
    public GraphNode getFrom() {
        return from;
    }

    /**
     * Visszaadja az a csúcsot, amibe beérkezik az él
     * 
     * @return
     */
    public GraphNode getTo() {
        return to;
    }

    /**
     *Visszaadja a súlyt
     * 
     * @return
     */
    public double getWeight() {
        return weight;
    }

    public void resize(double x1, double y1, double x2, double y2){

    }

    public Line getLine(){
        return line;
    }

    public void setColor(Color c){
        line.setStroke(c);
    }

//    public void reposition(double startx, double starty, double endx, double endy){
//        if(startx==0 && starty==0){
//            line.setEndX(endx);
//            line.setEndY(endy);
//        }else if(endx==0 && endy==0){
//            line.setStartX(startx);
//            line.setStartY(starty);
//        }
//    }
    @Override
    public int hashCode(){
        int hash=3;
        hash=97*hash+Objects.hashCode(this.type);
        hash=97*hash+Objects.hashCode(this.from);
        hash=97*hash+Objects.hashCode(this.to);
        return hash;
    }

    @Override
    public boolean equals(Object obj){
        if(obj==null){
            return false;
        }
        if(getClass()!=obj.getClass()){
            return false;
        }
        final GraphEdge other=(GraphEdge)obj;
        if(this.type!=other.type){
            return false;
        }
        if(!Objects.equals(this.from, other.from)){
            return false;
        }
        if(!Objects.equals(this.to, other.to)){
            return false;
        }
        return true;
    }

    public void relocate(){
        weightCircle.setCenterX((from.getCenterX()+to.getCenterX())/2.);
        weightCircle.setCenterY((from.getCenterY()+to.getCenterY())/2);
        p.getPoints().clear();
        
        
        repositionLabel();
    }
}
