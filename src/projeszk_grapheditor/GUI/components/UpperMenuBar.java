/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package projeszk_grapheditor.GUI.components;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.event.EventHandler;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import projeszk_grapheditor.misc.Constants;
import projeszk_grapheditor.misc.EditModes;

/**
 *
 * @author joci
 */
public class UpperMenuBar{
    private final int BUTTON_HEIGHT=60, BUTTON_WIDTH=70, BUTTON_COUNT=3;
    private HBox modePane;
    private ToggleButton node, edge, dummy;

    public UpperMenuBar(){
        modePaneSetup();
        
        
        ButtonsSetup();
        
        
        
    }

    private void ButtonsSetup(){
        ToggleGroup tg = new ToggleGroup();
        node = new ToggleButton("Node");
        edge = new ToggleButton("Edge");
        dummy = new ToggleButton("Dummy");
        buttonSetup(node, BUTTON_HEIGHT, BUTTON_WIDTH, tg, "toggleButtonLeft");
        buttonSetup(edge, BUTTON_HEIGHT, BUTTON_WIDTH, tg, "toggleButtonInner");
        buttonSetup(dummy, BUTTON_HEIGHT, BUTTON_WIDTH, tg, "toggleButtonRight");
        
        node.setSelected(true);
    }

    private void modePaneSetup(){
        modePane = new HBox();
        modePane.setPrefHeight(60);
        modePane.setPrefWidth(210);
        modePane.setTranslateY(-45);
        modePane.setOnMouseEntered(new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent e){
                final Timeline timeline=new Timeline();
                final KeyValue kv=new KeyValue(modePane.translateYProperty(), 0);
                final KeyFrame kf=new KeyFrame(Duration.millis(150), kv);
                timeline.getKeyFrames().add(kf);
                timeline.play();
            }
        });
        modePane.setOnMouseExited(new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent e){
                final Timeline timeline=new Timeline();
                final KeyValue kv=new KeyValue(modePane.translateYProperty(), -45);
                final KeyFrame kf=new KeyFrame(Duration.millis(200), kv);
                timeline.getKeyFrames().add(kf);
                timeline.play();
            }
        });
        modePane.getStyleClass().add("upper-menu");
    }

    public HBox getModePane(){
        return modePane;
    }  
    
    public EditModes getMode(){
        if(node.isSelected()){
            return EditModes.NODE;
        }else if(edge.isSelected()){
            return EditModes.EDGE;
        }else if(dummy.isSelected()){
            return EditModes.DUMMY;
        }else{
            throw new IllegalStateException("Unable to detect MODE!");
        }
    }

    private void buttonSetup(ToggleButton button, int height, int width, ToggleGroup tg, String styleClass){
        button.setPrefHeight(height);
        button.setPrefWidth(width);
        button.setToggleGroup(tg);
        button.getStyleClass().add(styleClass);
        modePane.getChildren().add(button);
    }
    
}
