/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeszk_grapheditor.GUI;

import projeszk_grapheditor.misc.NodeStages;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.util.Duration;
import projeszk_grapheditor.GUI.components.UpperMenuBar;
import projeszk_grapheditor.algorithm.Dijkstra;
import projeszk_grapheditor.data.Graph;
import projeszk_grapheditor.data.GraphEdge;
import projeszk_grapheditor.data.GraphNode;
import projeszk_grapheditor.data.Type;
import projeszk_grapheditor.misc.TitleGenerator;

/**
 *
 * @author Pollák József
 * @author Komáromi Zsófia
 */
public class FXMLMainController implements Initializable{

    private long lastMove, slideAnimation=200;
    private boolean isMenuPaneHidden;
    private double circleDiameter=20;
    private NodeStages stage;
    private GraphNode selectedNode;
    private GraphNode fromNode, toNode;
    private Line tempLine;

    private Graph graph;

    private double clickX1, clickY1, clickX2, clickY2, zoomFactor;
    private UpperMenuBar upperMenu;

    @FXML private AnchorPane basePane;
    @FXML private AnchorPane graphPane;
    @FXML private AnchorPane slidePane;
    @FXML private FlowPane menuContainer;
    @FXML private Button btnCollapse;
    @FXML private Button btnDijkstra;
    @FXML private TextArea helpArea;
    @FXML private TextArea graphArea;

    /**
     * <p>Az ablak felső részén lévő kapcsoló figyelője. Két mód közül választhatunk, Node(csúcs) és Edge(él).
     * Ha a Node módot választjuk, akkor új csúcs helyezhető le, a meglévő csúcsok mozgathatók vagy törölhetők. 
     * Ha az Edge módot választjuk, akkor két csúcs közé lehet élt behúzni.</p>
     * 
     * @param e 
     */
    @FXML private void onMousePressed(MouseEvent e){

        clickX1=e.getX();
        clickY1=e.getY();
        switch(upperMenu.getMode()){
            case NODE:
                if(e.isPrimaryButtonDown()&&e.isControlDown()&&e.getTarget().getClass().equals(Circle.class)){
                    deleteNode(e);
                }else if(e.isPrimaryButtonDown()&&e.getTarget().getClass().equals(Circle.class)){
                    selectNode(e);
                }else if(e.getButton().equals(MouseButton.SECONDARY)||e.getButton().equals(MouseButton.MIDDLE)){
                    stage=NodeStages.MOVE_VIEW;
                }else if(e.getClickCount()==2&&e.isPrimaryButtonDown()){
                    addNode();
                }else if(e.getButton().equals(MouseButton.PRIMARY)&&e.getTarget().getClass().equals(AnchorPane.class)){
                    deselectNode(e);
                }
                break;
            case EDGE:
                if(e.isPrimaryButtonDown()&&e.getTarget().getClass().equals(Circle.class)){
                    GraphNode tempNode=graph.getSelected((Circle)e.getTarget());
                    if(tempNode!=null){
                        clickX1=tempNode.getCenterX();
                        clickY1=tempNode.getCenterY();
                        stage=NodeStages.ADD_EDGE;
                        fromNode=tempNode;
                        tempLine.setStartX(clickX1);
                        tempLine.setStartY(clickY1);
                        tempLine.setEndX(clickX1);
                        tempLine.setEndY(clickY1);
                    }
                }else if(e.getButton().equals(MouseButton.SECONDARY)||e.getButton().equals(MouseButton.MIDDLE)){
                    stage=NodeStages.MOVE_VIEW;
                }
                break;
            case DUMMY:
                break;
        }

    }

    /**
     * A csúcs kiválasztásáért felelős függvény
     * @param e 
     */
    private void deselectNode(MouseEvent e){
        graph.removeZeroScale(graphPane);
        if(graphPane==(AnchorPane)e.getTarget()){
            if(selectedNode!=null){
                selectedNode.unselect();
                selectedNode=null;
            }
        }
    }

    /**
     * Ezzel a függvénnyel új csúcsot adhatok a felülethez.
     */
    private void addNode(){
        for(GraphNode g : graph.getNodes()){
            g.repositionLabel();
        }
        GraphNode gn=new GraphNode(clickX1, clickY1, circleDiameter, TitleGenerator.getNext()+"", Graph.generateID());
        graphPane.getChildren().addAll(gn.allNodes());
        graph.addNode(gn);
    }

    private void selectNode(MouseEvent e){
        GraphNode tempNode=graph.getSelected((Circle)e.getTarget());
        if(tempNode!=null){
            if(selectedNode!=tempNode){
                if(selectedNode!=null){
                    selectedNode.unselect();
                }
                selectedNode=tempNode;
                selectedNode.setSelected(true);
            }
            stage=NodeStages.MOVE_NODE;
        }
    }

    /**
     * Csúcs törlésekor lefut egy animáció, ami az eltűnést szimulálja és törlődik a csúcs a Graph osztályból
     * @param e 
     */
    private void deleteNode(MouseEvent e){
        graph.removeZeroScale(graphPane);
        GraphNode tempNode=graph.getSelected((Circle)e.getTarget());
        if(tempNode!=null){
            tempNode.removeAnimation();
        }
        graph.removeNode(tempNode);
    }

     /**
     * <p>Az ablak felső részén lévő kapcsoló figyelője. Két mód közül választhatunk, Node(csúcs) és Edge(él).
     * Ha a Node módot választjuk, akkor új csúcs helyezhető le, a meglévő csúcsok mozgathatók vagy törölhetők. 
     * Ha az Edge módot választjuk, akkor két csúcs közé lehet élt behúzni.</p>
     * 
     * @param e 
     */
    @FXML
    private void onMouseDragged(MouseEvent e){
        switch(upperMenu.getMode()){
            case NODE:
                if(stage.equals(NodeStages.MOVE_VIEW)){
                    moveAllNodes(e);
                }else if(stage.equals(NodeStages.MOVE_NODE)){
                    moveSelectedNode(e);
                }
                break;
            case EDGE:
                if(stage.equals(NodeStages.ADD_EDGE)){
                    clickX2=e.getX();
                    clickY2=e.getY();
                    GraphNode tempNode=graph.overNode(clickX2, clickY2, circleDiameter);
                    if(tempNode!=null){
                        toNode=tempNode;
                        tempLine.setEndX(toNode.getCenterX());
                        tempLine.setEndY(toNode.getCenterY());
                    }else{
                        toNode=null;
                        tempLine.setEndX(clickX2);
                        tempLine.setEndY(clickY2);
                    }

                }else if(stage.equals(NodeStages.MOVE_VIEW)){
                    moveAllNodes(e);
                }
        }
    }

    private void moveAllNodes(MouseEvent e){
        clickX2=e.getX();
        clickY2=e.getY();
        for(GraphNode gn : graph.getNodes()){
            gn.relocate(gn.getCenterX()+clickX2-clickX1, gn.getCenterY()+clickY2-clickY1);
            gn.relocateAllEdges();
        }
        clickX1=clickX2;
        clickY1=clickY2;
    }

    private void moveSelectedNode(MouseEvent e){
        clickX2=e.getX();
        clickY2=e.getY();
        selectedNode.relocate(selectedNode.getCenterX()+clickX2-clickX1, selectedNode.getCenterY()+clickY2-clickY1);
        selectedNode.relocateAllEdges();
        clickX1=clickX2;
        clickY1=clickY2;
    }

     /**
     * <p>Az ablak felső részén lévő kapcsoló figyelője. Két mód közül választhatunk, Node(csúcs) és Edge(él).
     * Ha a Node módot választjuk, akkor új csúcs helyezhető le, a meglévő csúcsok mozgathatók vagy törölhetők. 
     * Ha az Edge módot választjuk, akkor két csúcs közé lehet élt behúzni.</p>
     * 
     * @param e 
     */
    @FXML
    private void onMouseReleased(MouseEvent e){
        switch(upperMenu.getMode()){
            case NODE:
                break;
            case EDGE:
                if(stage.equals(NodeStages.ADD_EDGE)){
                    if(fromNode!=null&&toNode!=null&&fromNode!=toNode){
                        //Math.random() nem lehet nulla, és nem lehet ugyanolyan szám, mint ami már benne van az élek listájában

                        final GraphEdge ge=new GraphEdge(fromNode, toNode, (double)(int)(Math.random()*500), Type.UNDIRECTED_WEIGHTED);

                        if(!graph.containsEdge(ge)){
                            List<? extends Node> edgeNodes=ge.allNodes();
                            graphPane.getChildren().addAll(edgeNodes);
                            Collections.reverse(edgeNodes);
                            for(Node n : edgeNodes){
                                n.toBack();
                            }
                            graph.addEdge(ge);
                            ge.relocate();
                            
                        }
                    }
                    resetTempLine();
                }
                break;
            case DUMMY:
                break;
        }
        stage=NodeStages.EMPTY;
        graphArea.setText(graph.toString());

    }

    @FXML
    private void onMouseScrolled(ScrollEvent e){
//        double clickX=e.getX();
//        double clickY=e.getY();
//        if(e.getDeltaY()>0){
//            zoomFactor=.1;
//        }else{
//            zoomFactor=-.1;
//        }
//        circleDiameter=circleDiameter+circleDiameter*zoomFactor;
//        for(Circle c : circles){
//            c.setCenterX(c.getCenterX()+(c.getCenterX()-clickX)*zoomFactor);
//            c.setCenterY(c.getCenterY()+(c.getCenterY()-clickY)*zoomFactor);
//            c.setRadius(circleDiameter);
//        }
//        e.consume();
    }

    /**
     * <p>Ez a függvény két gombot figyel, a jobb oldali panel csúsztató gombját és Dijkstra algoritmus lefutását indító gombot.</p>
     * @param e 
     */
    @FXML
    private void onButtonClick(ActionEvent e
    ){
        Button b=(Button)e.getSource();
        if(b.equals(this.btnCollapse)){
            slideMenu();
        }

        if(b.equals(this.btnDijkstra)){
            Dijkstra.DijkstraAlgorithm(graph);
        }
    }

    /**
     * <p>A függvény animálást végez, a jobb oldali panel elcsúsztatását, eltüntetését és visszahozását játsza le.</p>
     */
    private void slideMenu(){
        lastMove=0;
        int direction;
        if(isMenuPaneHidden){
            direction=-1;
        }else{
            direction=1;
        }
        int border=0;
        final Timeline timeline=new Timeline();
        final KeyValue kv1=new KeyValue(slidePane.layoutXProperty(), slidePane.getLayoutX()+direction*(slidePane.getPrefWidth()+border));
        final KeyFrame kf1=new KeyFrame(Duration.millis(slideAnimation), kv1);
        final KeyValue kv2=new KeyValue(btnCollapse.layoutXProperty(), btnCollapse.getLayoutX()+direction*(slidePane.getPrefWidth()+border));
        final KeyFrame kf2=new KeyFrame(Duration.millis(slideAnimation), kv2);
        timeline.getKeyFrames().add(kf1);
        timeline.getKeyFrames().add(kf2);
        timeline.play();
        isMenuPaneHidden=!isMenuPaneHidden;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb){
        lastMove=0;
        isMenuPaneHidden=false;
        stage=NodeStages.EMPTY;
        basePane.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>(){
            @Override
            public void handle(KeyEvent t){
                if(t.getCode().equals(KeyCode.SPACE)&&t.isControlDown()&&System.currentTimeMillis()-lastMove-10>slideAnimation){
                    slideMenu();
                    lastMove=System.currentTimeMillis();
                }
            }
        });

        upperMenu=new UpperMenuBar();
        menuContainer.getChildren().add(upperMenu.getModePane());

        graph=new Graph();

        tempLine=new Line();
        graphPane.getChildren().add(tempLine);

        //helpArea.getStyleClass().add("text-area");
    }

    private void resetTempLine(){
        tempLine.setStartX(-10);
        tempLine.setStartY(-10);
        tempLine.setEndX(-10);
        tempLine.setEndY(-10);
    }

}
